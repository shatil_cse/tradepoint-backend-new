import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;

@SuppressWarnings("serial")
public class TakeNotification extends HttpServlet{
	int count=0;
	String title="Not notified yet..";	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain; charset=utf8");
		PrintWriter out = resp.getWriter();
		out.println(title);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		count++;
		title="Count:"+count+"\n";
		//doGet(req, resp);
		resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		JSONArray search_result=new JSONArray();
		JSONArray jsonarr=new JSONArray();
		jsonarr=comutil.getJSONarray(req,out);
		DB db=comutil.getdb(out,getServletContext());
		int len=jsonarr.length();
		for(int i=0;i<len;i++)
		{
			boolean text_search=false;
			String index_name="tag_name";
			String collection_name="notification_collection";
			JSONObject jsonobj=new JSONObject();
			try {
				jsonobj = jsonarr.getJSONObject(i);
				title=title+"\nJsonobject----------\n"+jsonobj.toString()+"\n-------end of jsonobject--------\n";
				text_search=jsonobj.getBoolean("text_search");
				index_name=jsonobj.getString("index_name");
				System.out.println(title);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try{
				if(text_search==false) search_result=comutil.brawsingdatabase(db,collection_name,search_result,jsonobj);
				else search_result=comutil.searching_text(db,collection_name,search_result,jsonobj,index_name);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		JSONObject rset=new JSONObject();
		try {
			title=title+search_result.toString()+"\n---------------------------------------------OPTMize object--------------\n";			
			JSONArray unopt_search_result=new JSONArray(search_result.toString());
			JSONArray opt_search_result=comutil.optimize_json_array(unopt_search_result);
			rset.put("notification_time", System.currentTimeMillis());
			rset.put("search_result", opt_search_result);
			if(opt_search_result.length()==0) rset.put("status_int", 99);
			else rset.put("status_int", 0);
			title=title+opt_search_result.toString();
			//rset.put("search_result", comutil.optimize_json_array(search_result));
			} catch (JSONException e) {
			// TODO Auto-generated catch block
				try {
					rset.put("status_int", 500);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			e.printStackTrace();
		}
		
		out.write(rset.toString());
	}
}
