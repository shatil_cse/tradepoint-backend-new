
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

@SuppressWarnings("serial")
public class Report_messages extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// Set response content type
		resp.setContentType("text/plain; charset=utf8");
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		DBCollection collection=db.getCollection("report_messages");
		DBCursor cur=collection.find();
		while(cur.hasNext())
		{
			try {
				JSONObject jobj=new JSONObject(cur.next().toString());
				out.println("_id"+jobj.getLong("_id")+"User"+jobj.getString("user_id")+"\nMessage:"+jobj.getString("message_body")+"\n"+jobj.getLong("time")+"\n\n");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		JSONObject rset=new JSONObject();
		try {
		long time=System.currentTimeMillis();
		rset.put("notification_time",time);
		rset.put("status_int", 500);
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		DBCollection collection=db.getCollection("report_messages");
		long count=collection.getCount();
		JSONObject jobj=comutil.getJSON(req, out);
		jobj.put("_id", count);
		jobj.put("time", time);
		DBObject statement=(DBObject) JSON.parse(jobj.toString());
		collection.insert(statement);
		rset.put("status_int", 260);
		out.write(rset.toString());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			out.write(rset.toString());
		}
	}	
}
