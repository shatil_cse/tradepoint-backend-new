import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

@SuppressWarnings("serial")
public class Profile extends HttpServlet{
	JSONObject result=new JSONObject();
	String errors="no_error";
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		DBCollection usercoll = db.getCollection("user_info");
		DBCursor cur=usercoll.find();
		out.println(errors);
		while(cur.hasNext())
		{
			out.println(cur.next().toString());
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	  resp.setContentType("application/json; charset=utf8");
	  errors="no_error";
	  PrintWriter out = resp.getWriter();
	  CommonUtilities comutil=new CommonUtilities();
	  String str[]=comutil.setdatetime();
		try {
			result.put("date", str[0]);
			result.put("time", str[1]);
			result.put("status_int", 500);
			JSONObject jsonobj=comutil.getJSON(req,out);
			String reg_id=jsonobj.getString("reg_id");
			DB db=comutil.getdb(out,getServletContext());
			String _id=jsonobj.getString("_id");
			
			if(check_reg_id(reg_id,_id,db))
			{
				jsonobj.remove("reg_id");
				if(add_to_profile(jsonobj,db))
				{
					result.put("status_int", 1111);
					result.put("message_code", 0);
				}
				else result.put("message_code", 201);
			}
			else result.put("message_code", 301);
		} catch (Exception e) {
			errors+="\nondopost:"+e.toString();
			out.println("Error In Posting your Request:\n"+e.toString());
		}
		errors+="\nResult:"+result.toString();
		out.write(result.toString());
	}

	private boolean add_to_profile(JSONObject jsonobj,DB db) {
		// TODO Auto-generated method stub
		try{
			DBCollection usercoll = db.getCollection("user_info");
			DBObject query=(DBObject) JSON.parse(new JSONObject().put("_id", jsonobj.getString("_id")).toString());
			// convert JSON to DBObject directly
			DBObject new_data = (DBObject) JSON.parse(jsonobj.toString());
			//usercoll.insert(new_data);
			usercoll.update(query, new_data);
			result.put("user_info", usercoll.findOne(query));
			return true;
		}catch(Exception e)
		{
			errors+="\nonaddprofile:"+e.toString();
			return false;
		}
	}

	private boolean check_reg_id(String reg_id, String _id,DB db){
		// TODO Auto-generated method stub
		try{
			JSONObject jobj=new JSONObject();
			jobj.put("reg_id", reg_id);
			jobj.put("_id", _id);
			DBObject statement=(DBObject)JSON.parse(jobj.toString());
			JSONObject json=new JSONObject(db.getCollection("registration").findOne(statement).toString());
			if(json.getString("reg_id").equals(reg_id))return true;
			else return false;
		}
		catch(Exception e)
		{
			return false;
		}
	}
}
