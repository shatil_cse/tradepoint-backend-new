
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.mongodb.DB;
import com.sun.mail.iap.Response;

@SuppressWarnings("serial")
public class Updatedocs extends HttpServlet{
	int count=0;
	String messsage="";
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Recoveryoperation rp=new Recoveryoperation(response);
		PrintWriter out=response.getWriter();
		out.println(count+" "+messsage+"\n");
	}
	@Override
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			// TODO Auto-generated method stub
		  resp.setContentType("application/json; charset=utf8");
			PrintWriter out = resp.getWriter();
			CommonUtilities comutil=new CommonUtilities();
			Recoveryoperation rp=new Recoveryoperation(resp);
			DB db=comutil.getdb(out,getServletContext());
			try {
				JSONObject jsonobj=new JSONObject().put("status_int", 270);
				messsage=jsonobj.toString();
				jsonobj=comutil.getJSON(req, out);
				if(rp.check_reg_id(jsonobj.getString("reg_id"), jsonobj.getString("_id"), db))
				{
					int type=jsonobj.getInt("data_change_st");
					if(type==0) messsage=rp.save_reg(db,jsonobj,out);
					else if(type>0&&type<6) messsage=rp.change_data(db,jsonobj,out);
					else if(type==8) messsage=rp.recover_device(db, jsonobj, out);
					else if(type==9) messsage=rp.lost_device_recovery(db, jsonobj, out);
				}
				else out.write(messsage);
			} catch (Exception e) {
				e.printStackTrace();
			}
	  }
}
