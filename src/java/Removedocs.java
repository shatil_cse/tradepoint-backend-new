
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("serial")
public class Removedocs extends HttpServlet{
	  @Override
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			// TODO Auto-generated method stub
		  resp.setContentType("application/json; charset=utf8");
			PrintWriter out = resp.getWriter();
			CommonUtilities comutil=new CommonUtilities();
			JSONObject rset=new JSONObject();
			try {
				rset.put("status_int", 500);
				JSONObject jsonobj;
				jsonobj=comutil.getJSON(req, out);
				JSONObject statement=jsonobj.getJSONObject("statement");
				comutil.removedocuments(comutil.getdb(out,getServletContext()),statement.toString(),jsonobj.getString("catagory"));
				rset.put("notification_time", System.currentTimeMillis());
				rset.put("status_int", 200);
			} catch (Exception e) {
				e.printStackTrace();
			}
			out.write(rset.toString());
	  }
}
