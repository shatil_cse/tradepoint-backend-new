import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

@SuppressWarnings("serial")
public class Send_message extends HttpServlet{
	JSONObject result=new JSONObject();
	JSONObject message_obj=new JSONObject();
	int review=0;
	String error="no_error";
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		DBCollection usercoll = db.getCollection("message_collection");
		DBCursor cur=usercoll.find();
		out.println(error+"\n-----------------------------------------");
		while(cur.hasNext())
		{
			out.println(cur.next().toString());
		}
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setContentType("application/json; charset=utf8");
		error="no_error";
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		String str[]=comutil.setdatetime();
		try {
			result=new JSONObject();
			result.put("date", str[0]);
			result.put("time", str[1]);
			result.put("status_int", 500);
			message_obj=new JSONObject();
			JSONObject json=comutil.getJSON(req, out);
			result.put("message", json);
			String _id=json.getString("from");
			String reg_id=json.getString("reg_id");
			json.remove("reg_id");
			if(get_registration_id(_id, db).equals(reg_id))
			{
				if(check_confirm(json,db))
				{
					if(check_profile(db)&&set_details(json,db))
					{
						//change the value of json ............................................................................
						if(send_message_operation(comutil,db))
						{
							result.put("status_int", 1);
							result.put("message_code", 0);
						}
						else result.put("message_code", 201);
					}
					else result.put("message_code", 301);
				}
				else result.put("message_code", 401);
			}
			else result.put("message_code", 501);
		} catch (Exception e) {
			error+="\nOndopost:"+e.toString();
			out.println("Error In Posting your Request:\n"+e.toString());
		}
		error+="\nResult:"+result.toString();
		out.write(result.toString());
	}
	private boolean check_confirm(JSONObject json,DB db) throws JSONException {
		// TODO Auto-generated method stub
		boolean ret=true;
		int confirm=json.getInt("confirm");
		if(confirm==5) review=1;
		else review=0;
		JSONObject result_mes=new JSONObject();
		JSONObject result_mes2=new JSONObject();
		DBCollection usercoll = db.getCollection("message_collection");
		String _id=json.getString("from")+json.getString("to")+json.getString("_id");
		result_mes.put("_id", _id);
		DBObject query=(DBObject) JSON.parse(new JSONObject().put("_id", _id).toString());
		_id=json.getString("to")+json.getString("from")+json.getString("_id");
		result_mes2.put("_id", _id);
		DBObject query2=(DBObject) JSON.parse(new JSONObject().put("_id", _id).toString());
		try{
			try{
				result_mes=new JSONObject(usercoll.findOne(query).toString());
			}catch(Exception e)
			{
				//for first time it will get null pointer exception...........
			}
			try{
				result_mes2=new JSONObject(usercoll.findOne(query2).toString());
			}catch(Exception e)
			{
				
			}
			int tem_con=0;
			if(result_mes.has("confirm")) tem_con=result_mes.getInt("confirm");
			int tem_con2=0;
			if(result_mes2.has("confirm")) tem_con2=result_mes2.getInt("confirm");
			boolean can_send=true;
			//if(result_mes.has("can_send")) can_send=result_mes.getBoolean("can_send");
			if(confirm==1)
			{
				if(tem_con==1||tem_con==2) ret=false;
				if(tem_con2==1&&tem_con==0)
				{
					result_mes.put("confirm", 2);
					result_mes2.put("confirm", 2);
					tem_con=2;
					tem_con2=2;
				}
				else if(tem_con==0)
				{
					result_mes.put("confirm", 1);
					result_mes2.put("confirm", tem_con2);
					tem_con=1;
				}
			}
			else if(confirm==5&&tem_con==2)
			{
				result_mes.put("confirm", 5);
				result_mes2.put("confirm", tem_con2);
				tem_con=5;
			}
			else if(confirm==0&&can_send)
			{
				result_mes.put("can_send", false);
				result_mes2.put("can_send", true);
			}
			else ret=false;
			if(tem_con2==5&&tem_con==5)
			{
				tem_con=0;
				tem_con2=0;
				can_send=true;
				result_mes.put("can_send", true);
				result_mes2.put("can_send", true);
			}
			JSONArray message_body=new JSONArray();
			if(result_mes.has("message_body")) message_body=result_mes.getJSONArray("message_body");
			message_body.put(message_body.length(),json.getString("message_body"));
			result_mes.put("message_body", message_body);
			json.put("confirm", tem_con);
			json.put("can_send", can_send);
			result.put("message", json);
			json.put("confirm", tem_con2);
			message_obj.put("message", json);
			if(ret)
			{
				usercoll.save((DBObject)JSON.parse(result_mes.toString()));
				usercoll.save((DBObject)JSON.parse(result_mes2.toString()));
			}
			//initialize result and message_obj............................
		}catch(Exception e)
		{
			error+="\nConfirm_check"+e.toString();
			return false;
		}
		error+="\n--------------\nret="+ret+"\n-------------------\n";
		return ret;
	}
	private boolean set_details(JSONObject json,DB db) {
		// TODO Auto-generated method stub
		try{
		DBCollection usercoll = db.getCollection("searchdet"+json.getString("catagory"));
		DBObject query=(DBObject) JSON.parse(new JSONObject().put("_id", json.getString("_id")).toString());
		// convert JSON to DBObject directly
		JSONObject det=new JSONObject(usercoll.findOne(query).toString());
		//error+="\nDEtails:"+det.toString();
		if(!det.has("_id")) return false;
		result.put("details", det);
		message_obj.put("details", det);
		return true;
		}catch(Exception e)
		{
			error+="\nSet_details:"+e.toString();
			return false;
		}
	}
	public String get_registration_id(String _id,DB db)
	{
		try{
			JSONObject jobj=new JSONObject();
			jobj.put("_id", _id);
			DBObject statement=(DBObject)JSON.parse(jobj.toString());
			JSONObject json=new JSONObject(db.getCollection("registration").findOne(statement).toString());
			return json.getString("reg_id");
		}
		catch(Exception e)
		{
			return "";
		}
	}
	private boolean check_profile(DB db) {
		// TODO Auto-generated method stub
		try{
			String _id="";
			JSONObject message=result.getJSONObject("message");
			if(review==1)
			{
				_id=message.getString("to");
			}
			else _id=message.getString("from");
			DBCollection usercoll = db.getCollection("user_info");
			DBObject query=(DBObject) JSON.parse(new JSONObject().put("_id", _id).toString());
			JSONObject juser=new JSONObject(usercoll.findOne(query).toString());
			if(review==1)
			{
				float review=message.getInt("review");
				float success=0,unsuccess=0,bad=0;
				if(review==4) success=1;
				else if(review==2) bad=1;
				else if(review==0) unsuccess=1;
				if(juser.has("successfull")) success+=juser.getInt("successfull")+bad;
				if(juser.has("unsuccessfull")) unsuccess+=juser.getInt("unsuccessfull");
				if(juser.has("bad")) bad+=juser.getInt("bad");
				float rating=0;
				rating=(success/10)-(bad/10);
				if(rating>50) rating=50;
				rating=rating+(success*50/(unsuccess+success));
				int lavel=1;
				if(rating<30) lavel=1;
				else if(rating<50) lavel=2;
				else if(rating<70) lavel=3;
				else if(rating<90) lavel=4;
				else lavel=5;
				juser.put("rating", (int)rating);
				juser.put("successfull", (int)success);
				juser.put("unsuccessfull", (int)unsuccess);
				juser.put("bad", (int)bad);
				juser.put("lavel", (int)lavel);
				usercoll.save((DBObject) JSON.parse(juser.toString()));
			}
			result.put("user_info", juser);
			message_obj.put("user_info", juser);
			return true;
		}catch(Exception e)
		{
			error+="\nUser_check"+e.toString();
			return false;
		}
	}
	public boolean send_message_operation(CommonUtilities comutil,DB db)
	  {
		Sender sender=comutil.newSender(getServletConfig());
		try {
			//get registration id
			String user_id=message_obj.getJSONObject("message").getString("to");
			String reg_id=get_registration_id(user_id, db);
			//think here...............
			JSONObject jobj=comutil.messagestatement(true, false, false, false,false,false);
			jobj.put("message", message_obj);
			Message message = comutil.build_message(jobj.toString());
		      Result result = sender.send(message, reg_id, 5);
			  String messageId = result.getMessageId();
		        String error = result.getErrorCodeName();
		        if(messageId!=null)
		        {
		        	//check and update canonical registration ids....
		        	return true;
		        }
		        else if (error.equals(Constants.ERROR_NOT_REGISTERED)||error.equals(Constants.ERROR_INVALID_REGISTRATION)) {
		          // application has been removed from device - unregister it
		        	return false;
		        }
		        else return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			error+="\nSend_message:"+e.toString();
			return false;
		}
	  }
}
