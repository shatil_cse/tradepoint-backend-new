
import org.json.JSONArray;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Shatil
 */
public class SearchUtilities {
    public SearchUtilities()
    {
        
    }
    public String get_formated_string(String name)
    {
       try{
        String name1=name.trim();
        int len=name1.length();
        char []carr=new char[len];
        int j=0;
        carr[j++]=name1.charAt(0);
        len--;
        for(int i=1;i<len;i++)
        {
                if(!(name1.charAt(i)=='a'||name1.charAt(i)=='e'||name1.charAt(i)=='i'||name1.charAt(i)=='o'||name1.charAt(i)=='u'))
                {
                        if(name1.charAt(i)!=name1.charAt(i-1)) carr[j++]=name1.charAt(i);
                }
        }
        carr[j++]=name1.charAt(len);
        String st=new String(carr,0,j);
        if(st.length()<2) return " ";
        return st;
        }catch(Exception e)
        {
                return " ";
        }
    }
    public JSONArray get_tagname(String name)
    {
            try{
            //making space separated..
            String st=name.replaceAll("\\.*[^a-zA-Z0-9]",",");
            st=st.trim();
            //spilting the all keys...
            String tags[]=st.split("\\s++");
            int len=tags.length;
            //generate nearest string for each key and to a final string with previous keys...

            String finalstring="";
            for(int i=0;i<len;i++)
            {
                    tags[i]=get_middle_characters_replaced(tags[i]);
                    finalstring=finalstring+" "+tags[i]+" "+get_formated_string(tags[i]);
                    finalstring=finalstring.trim();
            }
            finalstring=finalstring.trim();
            //spilting all the keywords....... 
            String finaltags[]=finalstring.split("\\s++");
            return getjarray(finaltags);
            }catch(Exception e)
            {
                    return new JSONArray();
            }
    }
    public String get_middle_characters_replaced(String name)
    {
            try{
            int len=name.length()-1;
            if(len<1) return name;
            String sub=name.substring(len,len+1);
            name=name.substring(0,len);
            name=name.replace('.', '_');
            name=name.replace('-', '_');
            name=name.concat(sub);
            return name;
            }catch(Exception e)
            {
                    return " ";
            }
    }
    public JSONArray getjarray(String array[])
    {
            JSONArray jarr=new JSONArray();
            int len=array.length;
            for(int i=0;i<len;i++)
            {
                    jarr.put(array[i]);
            }
            return jarr;
    }
    public String get_nin_saerch_staring(String name,boolean exactmatch)
    {
            try{
                    //making space separated..
                    String st=name.replace(',',' ')+" ";
                    if(exactmatch) return st.trim();
                    //spilting the all keys...
                    String tags[]=st.split("\\s++");
                    int len=tags.length;
                    //generate nearest string for each key and to a final string with previous keys...
                    String finalstring="";
                    for(int i=0;i<len;i++)
                    {
                            tags[i]=get_middle_characters_replaced(tags[i]);
                            finalstring=finalstring+" "+tags[i]+" "+get_formated_string(tags[i]);
                            finalstring=finalstring.trim();
                    }
                    return finalstring.trim();
            }catch(Exception e)
            {
                    return " ";
            }
    }

    public String get_and_regex_saerch_staring(String name)
    {
        try{
            name=name.replaceAll("\\.*[^a-zA-Z0-9]"," ");
            String []tags=name.split("\\s++");
            String regex="";
            for(int i=0;i<tags.length;i++) regex+="(?=.*"+tags[i]+")";
            return regex;
        }catch(Exception e)
        {
                return " ";
        }
    }
    public String get_or_regex_saerch_staring(String name)
    {
        try{
            name=name.replaceAll("\\.*[^a-zA-Z0-9]"," ");
            String []tags=name.split("\\s++");
            String regex=".*"+tags[0];
            for(int i=1;i<tags.length;i++) regex+="|.*"+tags[i];
            return regex;
        }catch(Exception e)
        {
                return "";
        }
    }
    public JSONArray get_in_saerch_array(String name)
    {
        JSONArray jsonArray=new JSONArray();
        try{
            name=name.replace("[^a-zA-Z0-9]",".*");
            return jsonArray.put(name);
        }catch(Exception e)
        {
            return jsonArray;
        }
    }
}
