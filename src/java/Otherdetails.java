import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONWriter;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@SuppressWarnings("serial")
public class Otherdetails extends HttpServlet {
	String lastresult="";
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		out.println("LAST RESULT:\n"+lastresult+"\n\n----------------------------");
		JSONObject rset=new JSONObject();
		try {
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		JSONArray jarr=new JSONArray();
		jarr.put("Electronics");
		jarr.put("Mobile Phones");
		String statement="{}";
		int len=jarr.length();
		for(int i=0;i<len;i++)
		{
			rset.put("status_int", 500);
			String catagory=jarr.getString(i);
			DBCollection collection=db.getCollection("otherdet"+catagory);
			DBObject st=(DBObject) JSON.parse(statement);
			DBCursor curr=collection.find(st);
			while(curr.hasNext())
			{
				out.println(curr.next());
			}
		}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			out.write(rset.toString());
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		JSONObject rset=new JSONObject();
		try {
		rset.put("status_int", 500);
		rset.put("notification_time", System.currentTimeMillis());
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		JSONObject jobj=comutil.getJSON(req, out);
		lastresult="\nGOT:"+jobj.toString()+"\n\n";
		JSONArray jarr=jobj.getJSONArray("catagory");
		JSONObject statement=jobj.getJSONObject("statement");
		DBObject st=(DBObject) JSON.parse(statement.toString());
		int len=jarr.length();
		JSONArray jres=new JSONArray();
		for(int i=0;i<len;i++)
		{
			String catagory=jarr.getString(i);
			DBCollection collection=db.getCollection("otherdet"+catagory);
			DBCursor curr=collection.find(st);
			while(curr.hasNext()) jres.put(new JSONObject(curr.next().toString()));
		}
		lastresult+="FIND RES:"+jres.toString();
		rset.put("status_int", 0);
		rset.put("search_result", jres);
		lastresult+="\n\nWritten RES:"+rset.toString()+"\n\n";
		out.write(rset.toString());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			out.write(rset.toString());
		}
	}
}