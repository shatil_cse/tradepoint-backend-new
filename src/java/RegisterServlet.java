/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

@SuppressWarnings("serial")
public class RegisterServlet extends HttpServlet {
	String result;
	int i=0;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		DBCollection regcoll = db.getCollection("registration");
		DBCursor cur=regcoll.find();
		out.print("Last Rseult: "+result+" Request number:"+i);
		while (cur.hasNext()) {
			out.println(cur.next().toString());
		}
	}

  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	  resp.setContentType("application/json; charset=utf8");
	  i++;
	  result="";
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		try {
			JSONObject jsonobj=comutil.getJSON(req,out);
			result+="Received:"+jsonobj.toString()+"\n";
			String status=validation(jsonobj.getString("reg_id"),comutil);
			if(status.startsWith("invalid"))
			{
				out.println("Registration ID is not valid");
				result+="Registration ID is not valid";
			}
			else if(status.startsWith("canonical"))
			{
				String gcm_id=status.substring(9, status.length());
				jsonobj.put("gcm_id", gcm_id);
				JSONObject res=addregid(comutil.getdb(out,getServletContext()),jsonobj,comutil);
				out.println(res.toString());
				result+="Canonical"+res.toString();
			}
			else
			{
				jsonobj.put("gcm_id", jsonobj.getString("reg_id"));
				JSONObject res=addregid(comutil.getdb(out,getServletContext()),jsonobj,comutil);
				out.println(res.toString());
				result+="Success"+res.toString();
			}
		} catch (Exception e) {
			out.println("Error In Posting your Request:\n"+e.toString());
			result+="Error In Posting your Request:\n"+e.toString();
		}
	}
  public String validation(String registrationId,CommonUtilities comutil)
  {
	Sender sender=comutil.newSender(getServletConfig());
	try {
		JSONObject jobj=comutil.messagestatement(false, true, false, false,false);
		jobj.put("message_body", "You have Successfully registered..");
		Message message = comutil.build_message(jobj.toString());
	      Result result = sender.send(message, registrationId, 5);
		  String messageId = result.getMessageId();
	        String error = result.getErrorCodeName();
	        if(messageId!=null)
	        {
	        	//return canonical registration Id..............
	        	if(result.getCanonicalRegistrationId()!=null)
	        	{
	        		return "canonical"+result.getCanonicalRegistrationId();
	        	}
	        	return "valid";
	        }
	        else if (error.equals(Constants.ERROR_NOT_REGISTERED)||error.equals(Constants.ERROR_INVALID_REGISTRATION)) {
		          // application has been removed from device - unregister it
		        	return "invalid";
		        }
	        else return "valid";
	        
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		return "invalid";
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		return "invalid";
	}
  }
	public JSONObject addregid (DB db,JSONObject objresutl,CommonUtilities comutil)
	{
		try {
			String str[]=comutil.setdatetime();
			objresutl.put("date", str[0]);
			objresutl.put("time", str[1]);
			DBCollection regcoll = db.getCollection("registration");
			// convert JSON to DBObject directly
			JSONObject jstatement=new JSONObject();
			jstatement.put("reg_id", objresutl.getString("reg_id"));
			DBObject statement=(DBObject) JSON.parse(jstatement.toString());
			DBCursor cur=regcoll.find(statement);
			if(cur.size()<0)//change to adjust the device registration........................................................
			{
				JSONObject jobj=new JSONObject(cur.next().toString());
				String user_id=jobj.getString("user_id");
			}
			else 
			{
				String user_id=create_user_id()+"inc"+regcoll.count();
				i++;
				objresutl.put("user_id", user_id);
				objresutl.put("_id", user_id);
				DBObject registration = (DBObject) JSON.parse(objresutl.toString());
				regcoll.insert(registration);
				JSONObject jsonobj=new JSONObject();
				jsonobj.put("_id", user_id);
				jsonobj.put("user_name",objresutl.getString("user_name"));
				add_to_profile(jsonobj, db);
			}
			return objresutl;
		} catch (Exception e) {
			return new JSONObject();
		}
	}
	private String create_user_id() {
		// TODO Auto-generated method stub
		return String.valueOf(System.currentTimeMillis());
	}

	private boolean add_to_profile(JSONObject jsonobj,DB db) {
		// TODO Auto-generated method stub
		try{
			DBCollection usercoll = db.getCollection("user_info");
			DBObject new_data = (DBObject) JSON.parse(jsonobj.toString());
			usercoll.save(new_data);
			return true;
		}catch(Exception e)
		{
			return false;
		}
	}
}
