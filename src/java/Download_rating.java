import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class Download_rating extends HttpServlet{

	String errors="";
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setContentType("text/plain; charset=utf8");
		PrintWriter out = resp.getWriter();
		out.print(errors);
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	  resp.setContentType("application/json; charset=utf8");
	  errors="no_error";
	  JSONObject result=new JSONObject();
	  PrintWriter out = resp.getWriter();
	  CommonUtilities comutil=new CommonUtilities();
	  try {
			result.put("status_int", 2221);
			JSONObject jsonobj=comutil.getJSON(req,out);
			DB db=comutil.getdb(out,getServletContext());
			DBObject statement=(DBObject) JSON.parse(jsonobj.getJSONObject("statement").toString());
			DBCollection usercoll = db.getCollection("user_info");
			DBCursor cur=usercoll.find(statement);
			JSONArray jarr=new JSONArray();
			JSONObject job;
			while(cur.hasNext()) 
			{
				job=new JSONObject(cur.next().toString());
				jarr.put(job);
			}
			result.put("user_info", jarr);
			result.put("status_int", 2222);
		} catch (Exception e) {
			errors+="dopost:"+e.toString();
			out.println("Error In Posting your Request:\n"+e.toString());
		}
		errors+="\nResult:"+result.toString();
		out.write(result.toString());
	}
}
