import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.util.JSON;
import javax.servlet.ServletContext;

public class CommonUtilities {
	String catagory[] = new String[] { "Furnitures", "Sports", "Electronics",
			"Mobile Phones", "Others" };
	static String dbname = "tradepoint", user = "admin", password = "hDaS-xaUkRhw";
	String databasename = "tradepoint";
	String server_url = "http://tradepoint-airship.rhcloud.com/tradepoint";
	String sender_id = "819466763396";
	Message BUILDED_MESSAGE;
	private static final int MULTICAST_SIZE = 1000;

	public CommonUtilities() {
		BUILDED_MESSAGE = new Message.Builder().build();
	}

	public Message build_message(String statement) {
		BUILDED_MESSAGE = new Message.Builder().delayWhileIdle(false)
				.timeToLive(100).addData("MESSAGE", statement).build();
		return BUILDED_MESSAGE;
	}

	public JSONObject messagestatement(boolean settings, boolean registration,
			boolean product_notification, boolean service_message,
			boolean upload_complete) {
		return messagestatement(false, settings, registration,
				product_notification, service_message, upload_complete);
	}

	public JSONObject messagestatement(boolean proposal, boolean settings,
			boolean registration, boolean product_notification,
			boolean service_message, boolean upload_complete) {
		JSONObject jsonobj = new JSONObject();
		try {
			jsonobj.put("proposal_message", proposal);
			jsonobj.put("enable_setting", settings);
			jsonobj.put("enable_registration", registration);
			jsonobj.put("enable_product", product_notification);
			jsonobj.put("enable_service", service_message);
			jsonobj.put("enable_upload_complete", upload_complete);
			return jsonobj;
		} catch (Exception e) {
			return jsonobj;
		}

	}

	public JSONObject getsettingobject() {
		JSONObject jsetting = new JSONObject();
		try {
			JSONArray jarr = new JSONArray(catagory);
			jsetting.put("catagory", jarr);
			jsetting.put("server_url", server_url);
			jsetting.put("sender_id", sender_id);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsetting;
	}

	public String[] setdatetime() {
		int day, month, year;
		GregorianCalendar date = new GregorianCalendar();
		day = date.get(Calendar.DAY_OF_MONTH);
		month = date.get(Calendar.MONTH);
		year = date.get(Calendar.YEAR);
		String strdate = String.format("%d :%d :%d", day, month, year);
		String strtime = String.format("%d :%d :%d", date.get(Calendar.HOUR),
				date.get(Calendar.MINUTE), date.get(Calendar.SECOND));
		return new String[] { strdate, strtime };
	}

	public JSONObject getJSON(HttpServletRequest request, PrintWriter out) {
		StringBuilder sb = new StringBuilder();
		BufferedReader br;
		try {
			br = request.getReader();
			String str;
			while ((str = br.readLine()) != null) {
				sb.append(str);
			}
			return new JSONObject(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			ShowErrorLogOfPostResuest.lastRespone+=e.toString();
                        out.println("ERROR IOEXCEPTION:" + e.toString());
			return new JSONObject();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			ShowErrorLogOfPostResuest.lastRespone+=e.toString();
			out.println("ERROR JSONEXCEPTION:" + e.toString());
			return new JSONObject();
		}
	}

	public JSONArray getJSONarray(HttpServletRequest request, PrintWriter out) {
		StringBuilder sb = new StringBuilder();
		BufferedReader br;
		try {
			br = request.getReader();
			String str;
			while ((str = br.readLine()) != null) {
				sb.append(str);
			}
			return new JSONArray(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			out.println("ERROR IOEXCEPTION:" + e.toString());
			return new JSONArray();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			out.println("ERROR JSONEXCEPTION:" + e.toString());
			return new JSONArray();
		}
	}

	protected Sender newSender(ServletConfig config) {
		String key = (String) config.getServletContext().getAttribute(
				ApiKeyInitializer.ATTRIBUTE_ACCESS_KEY);
		return new Sender(key);
	}

	public List<String> getdevices(DB db) {
		// TODO Auto-generated method stub
		List<String> devices = new ArrayList<String>();
		DBCollection coll = db.getCollection("registration");
		DBObject statement = (DBObject) JSON.parse("{}");
		DBCursor dbcur = coll.find(statement);
		while (dbcur.hasNext()) {
			String st = dbcur.next().toString();
			try {
				st = new JSONObject(st).getString("user_id");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// out.println(st);
			devices.add(st);
		}
		return devices;
	}

	public DB getdb(PrintWriter out,ServletContext context) {
		DB db;
		try {
                        //MongoClient mClient=new MongoClient(new MongoClientURI(System.getenv("OPENSHIFT_MONGODB_DB_URL")));
                        MongoClient mClient = (MongoClient) context.getAttribute("MONGO_CLIENT");
                        db=mClient.getDB(dbname);
			if (db.authenticate(user, password.toCharArray())) {
				return db;
			} else {
				try {
					if (db.isAuthenticated()) {
						out.println("Connection URL: " + System.getenv("OPENSHIFT_MONGODB_DB_URL") + "\ndbname: " + dbname
								+ "\nuser: " + user + "\npassword: " + password
								+ db.isAuthenticated());
						out.print("Connection failed");
						return null;
					}
				} catch (Exception e) {
                                    out.println("Crashed while authenticating"+e.toString());
				}
			}
			return db;
		} catch (Exception e) {
                    if(out!=null)
                    {
                        out.println("dbname: " + dbname + "\nuser: "
                                    + user + "\npassword: " + password);
                        out.println("AUTHENTICATION ERROR:" + e.toString());
                    }
                    db = null;
                    return db;
		}
	}

	/*public DB getdb() {
		DB db;
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(System.getProperty("user.home")
					+ "/mydb.cfg"));
			host = prop.getProperty("host").toString();
			dbname = prop.getProperty("dbname").toString();
			user = prop.getProperty("user").toString();
			password = prop.getProperty("pwd").toString();
			Mongo m = new Mongo(host, 27017);
			db = m.getDB(dbname);
			if (db.authenticate(user, password.toCharArray())) {
				return db;
			} else {
				try {
					if (prop.getProperty("flag").toString().equals("show")) {
						return null;
					}
				} catch (Exception e) {
					
				}
			}
			return db;
		} catch (Exception e) {
			db = null;
			return db;
		}
	}*/

	public void inserttodatabase(DB db, PrintWriter out, JSONObject objresutl) {
		try {
			String str[] = setdatetime();
			String strdate = str[0];
			String strtime = str[1];
			// get a single collection
			DBCollection scollection = db
					.getCollection(("searchdet" + objresutl
							.getString("catagory")));
			DBCollection fcollection = db.getCollection(("otherdet" + objresutl
					.getString("catagory")));
			DBCollection ncollection = db
					.getCollection("notification_collection");
			JSONObject serachdet = objresutl.getJSONObject("searchdet");
			JSONObject otherdet = objresutl.getJSONObject("otherdet");
			serachdet.put("date", strdate);
			serachdet.put("time", strtime);
			long currtime = System.currentTimeMillis();
			serachdet.put("curtime", currtime);
			// convert JSON to DBObject directly
			DBObject dbsearchdet = (DBObject) JSON.parse(serachdet.toString());
			scollection.insert(dbsearchdet);
			ncollection.insert(dbsearchdet);
			DBObject dbotherdet = (DBObject) JSON.parse(otherdet.toString());
			fcollection.insert(dbotherdet);

			JSONObject rset = new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			rset.put("status_int", 100);
			JSONArray search_result = new JSONArray();
			search_result.put(serachdet);
			rset.put("search_result", search_result);
			out.write(rset.toString());
		} catch (Exception e) {
			out.write(get_error_object().toString());
		}

	}

	public JSONObject get_error_object() {
		JSONObject rset = new JSONObject();
		try {
			rset.put("notification_time", System.currentTimeMillis());
			rset.put("status_int", 500);
			return rset;
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			return new JSONObject();
		}
	}

	public void brawsingdatabase(DB db, PrintWriter out, JSONObject jsonobj) {
		try {
			int len = catagory.length;
			JSONObject rset = new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			JSONArray search_result = new JSONArray();
			for (int i = 0; i < len; i++) {
				try {
					// get a single collection
					DBCollection collection = db.getCollection("searchdet"
							+ catagory[i]);
					DBObject dbobj = (DBObject) JSON.parse(jsonobj
							.getJSONObject("statement").toString());
					DBObject orderby = (DBObject) JSON.parse(new JSONObject()
							.put("curtime", -1).toString());
					DBCursor cursorDoc = collection.find(dbobj).addSpecial(
							"$orderby", orderby);
					while (cursorDoc.hasNext()) {
						search_result.put(cursorDoc.next());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (search_result.length() > 0) {
				rset.put("search_result", search_result);
				rset.put("status_int", 0);
			} else
				rset.put("status_int", 99);
			out.write(rset.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			out.write(get_error_object().toString());
		}
	}

	public void brawsingdatabase(DB db, String coll_name, PrintWriter out,
			JSONObject jsonobj) {
		try {
			// get a single collection
			DBCollection collection = db.getCollection(coll_name);
			DBObject dbobj = (DBObject) JSON.parse(jsonobj.getJSONObject(
					"statement").toString());
			DBObject orderby = (DBObject) JSON.parse(new JSONObject().put(
					"curtime", -1).toString());
			ShowErrorLogOfPostResuest.traceMessaage+="\n\nTrace DB Query: Queryobject"+dbobj.toString()+"\nOrdered by:"+orderby.toString()+"\n";
                        DBCursor cursorDoc = collection.find(dbobj).addSpecial("$orderby",
					orderby);
			// cursorDoc.sort(orderby);
			JSONObject rset = new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			JSONArray search_result = new JSONArray();
			while (cursorDoc.hasNext()) {
				search_result.put(cursorDoc.next());
			}
			if (search_result.length() > 0) {
				rset.put("search_result", search_result);
				rset.put("status_int", 0);
			} else
				rset.put("status_int", 99);
                     
                        ShowErrorLogOfPostResuest.lastRespone+=rset.toString();
			out.write(rset.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
                        ShowErrorLogOfPostResuest.errorMessages+="\nError on CommonUtils in browse data: "+e.toString();
			out.write(get_error_object().toString());
		}
	}

	public JSONArray brawsingdatabase(DB db, String coll_name,
			JSONArray search_result, JSONObject jsonobj) {
		try {
			// get a single collection
			DBCollection collection = db.getCollection(coll_name);
			DBObject dbobj = (DBObject) JSON.parse(jsonobj.getJSONObject(
					"statement").toString());
			DBObject orderby = (DBObject) JSON.parse(new JSONObject().put(
					"curtime", -1).toString());
			DBCursor cursorDoc = collection.find(dbobj).addSpecial("$orderby",
					orderby);
			// cursorDoc.sort(orderby);
			while (cursorDoc.hasNext()) {
				search_result.put(cursorDoc.next());
			}
			return search_result;
		} catch (Exception e) {
			return search_result;
		}
	}

	public void searching_text(DB db, String coll_name, PrintWriter out,
			JSONObject jsonobj, String index_name) {
            int version=2;
            if(version<3) 
            {
                searching_text_bellow_3(db, coll_name, out, jsonobj, index_name);
                return;
            }
		try {
			// get a single collection
			DBCollection collection = db.getCollection(coll_name);
                       // creates index ....
			DBObject crindex = (DBObject) JSON.parse(new JSONObject().put(
					index_name, "text").toString());
			//collection.createIndex(crindex);
                        collection.ensureIndex(crindex);

			JSONObject jsort = new JSONObject();
			jsort.put("score", new JSONObject().put("$meta", "textScore"));
			DBObject score = (DBObject) JSON.parse(jsort.toString());
			jsort.put("curtime", -1);
			DBObject orderby = (DBObject) JSON.parse(jsort.toString());

			// find documents.............
			DBObject dbobj = (DBObject) JSON.parse(jsonobj.getJSONObject(
					"statement").toString());
                        ShowErrorLogOfPostResuest.traceMessaage+="\n\nTrace DB Query: Queryobject"+dbobj.toString()+"\nOrdered by:"+score.toString()+"\n";
			DBCursor cursorDoc = collection.find(dbobj, score);

			// sort documents....................
			cursorDoc.sort(orderby);

			JSONObject rset = new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			JSONArray search_result = new JSONArray();
			while (cursorDoc.hasNext()) {
				search_result.put(cursorDoc.next());
			}
			if (search_result.length() > 0) {
				rset.put("search_result", search_result);
				rset.put("status_int", 0);
			} else
				rset.put("status_int", 99);
                        ShowErrorLogOfPostResuest.lastRespone+=rset.toString();
			out.write(rset.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
                        ShowErrorLogOfPostResuest.errorMessages+="\nError on CommonUtils in text search: "+e.toString();
			out.write(get_error_object().toString());
		}
	}
        public void searching_text_bellow_3(DB db, String coll_name, PrintWriter out,
			JSONObject jsonobj, String index_name) {
		try {
                    String filterName="";
                    JSONObject statement=new JSONObject();
                    JSONObject searchObject=null;
                    if(jsonobj.has("filter_name")) filterName=jsonobj.getString("filter_name");
                    if(jsonobj.has("statement")) statement=jsonobj.getJSONObject("statement");
                    if(filterName!=null&&filterName.length()>0)
                    {
                        searchObject=getSearchStatement(filterName, statement);
                    }
                    else 
                    {
                        searchObject=statement;
                    }
                    DBCollection collection = db.getCollection(coll_name);
                    JSONObject jsort = new JSONObject();
                    jsort.put("curtime", -1);
                    DBObject orderby = (DBObject) JSON.parse(jsort.toString());

                    // find documents.............
                    DBObject dbobj = (DBObject) JSON.parse(searchObject.toString());
                    ShowErrorLogOfPostResuest.traceMessaage+="\nSearch Queryobject"+dbobj.toString()+
                            "\nOrdered by:"+orderby.toString()+"\nSatatement:"+statement;
                    DBCursor cursorDoc = collection.find(dbobj);
                    // sort documents....................
                    cursorDoc.sort(orderby);
                    JSONObject rset = new JSONObject();
                    rset.put("notification_time", System.currentTimeMillis());
                    JSONArray search_result = new JSONArray();
                    while (cursorDoc.hasNext()) {
                            search_result.put(cursorDoc.next());
                    }
                    cursorDoc.close();
                    ShowErrorLogOfPostResuest.traceMessaage+="\nSearch Queryobject found:"+search_result.length()+" objects";
                    try{
                        if(filterName!=null&&filterName.length()>0)
                        {
                            JSONObject relatedSearchObject=getRelatedSearchStatement(filterName, searchObject, jsonobj.getJSONObject("statement"));
                            DBObject dbobj1 = (DBObject) JSON.parse(relatedSearchObject.toString());
                            DBCursor cursorDoc1 = collection.find(dbobj1);
                            // sort documents....................
                            cursorDoc1.sort(orderby);

                            while (cursorDoc1.hasNext()) {
                                    search_result.put(cursorDoc1.next());
                            }
                            cursorDoc1.close();
                            ShowErrorLogOfPostResuest.traceMessaage+="\nSearch total Queryobject found:"+search_result.length()+" objects";
                        }
                    }catch(Exception e)
                    {
                        ShowErrorLogOfPostResuest.errorMessages+="\nError on CommonUtils in related text search: "+e.toString();    
                    }
                    if (search_result.length() > 0) {
                            rset.put("search_result", search_result);
                            rset.put("status_int", 0);
                    } else
                            rset.put("status_int", 99);
                    ShowErrorLogOfPostResuest.lastRespone+=rset.toString();
                    out.write(rset.toString());
		} catch (Exception e) {
                    // TODO Auto-generated catch block
                    ShowErrorLogOfPostResuest.errorMessages+="\nError on CommonUtils in text search: "+e.toString();
                    out.write(get_error_object().toString());
		}
	}
        public JSONArray searching_text(DB db, String coll_name,
			JSONArray search_result, JSONObject jsonobj, String index_name) {
            int version=2;
            if(version<3)return searching_text_bellow_3(db, coll_name, search_result, jsonobj, index_name);
		try {
			// get a single collection
			DBCollection collection = db.getCollection(coll_name);
			// creates index ....
			DBObject crindex = (DBObject) JSON.parse(new JSONObject().put(
					index_name, "text").toString());
			collection.ensureIndex(crindex);

			JSONObject jsort = new JSONObject();
			jsort.put("score", new JSONObject().put("$meta", "textScore"));
			DBObject score = (DBObject) JSON.parse(jsort.toString());
			jsort.put("curtime", -1);
			DBObject orderby = (DBObject) JSON.parse(jsort.toString());

			// find documents.............
			DBObject dbobj = (DBObject) JSON.parse(jsonobj.getJSONObject(
					"statement").toString());
			DBCursor cursorDoc = collection.find(dbobj, score);

			// sort documents....................
			cursorDoc.sort(orderby);

			while (cursorDoc.hasNext()) {
				search_result.put(cursorDoc.next());
			}
			return search_result;
		} catch (Exception e) {
			return search_result;
		}
	}

	public JSONArray searching_text_bellow_3(DB db, String coll_name,
			JSONArray search_result, JSONObject jsonobj, String index_name) {
		try {
                    String filterName="";
                    JSONObject statement=new JSONObject();
                    JSONObject searchObject=null;
                    if(jsonobj.has("filter_name")) filterName=jsonobj.getString("filter_name");
                    if(jsonobj.has("statement")) statement=jsonobj.getJSONObject("statement");
                    if(filterName!=null&&filterName.length()>0)
                    {
                        searchObject=getSearchStatement(filterName, statement);
                    }
                    else 
                    {
                        searchObject=statement;
                    }
                    DBCollection collection = db.getCollection(coll_name);
                    JSONObject jsort = new JSONObject();
                    jsort.put("curtime", -1);
                    DBObject orderby = (DBObject) JSON.parse(jsort.toString());

                    // find documents.............
                    DBObject dbobj = (DBObject) JSON.parse(searchObject.toString());
                    ShowErrorLogOfPostResuest.traceMessaage+="\nSearch Queryobject"+dbobj.toString()+
                            "\nOrdered by:"+orderby.toString()+"\nSatatement:"+statement;
                    DBCursor cursorDoc = collection.find(dbobj);
                    // sort documents....................
                    cursorDoc.sort(orderby);
                    JSONObject rset = new JSONObject();
                    rset.put("notification_time", System.currentTimeMillis());
                    while (cursorDoc.hasNext()) {
                            search_result.put(cursorDoc.next());
                    }
                    cursorDoc.close();
                    ShowErrorLogOfPostResuest.traceMessaage+="\nSearch Queryobject found:"+search_result.length()+" objects";
                    try{
                        if(filterName!=null&&filterName.length()>0)
                        {
                            JSONObject relatedSearchObject=getRelatedSearchStatement(filterName, searchObject, jsonobj.getJSONObject("statement"));
                            DBObject dbobj1 = (DBObject) JSON.parse(relatedSearchObject.toString());
                            DBCursor cursorDoc1 = collection.find(dbobj1);
                            // sort documents....................
                            cursorDoc1.sort(orderby);

                            while (cursorDoc1.hasNext()) {
                                    search_result.put(cursorDoc1.next());
                            }
                            cursorDoc1.close();
                            ShowErrorLogOfPostResuest.traceMessaage+="\nSearch total Queryobject found:"+search_result.length()+" objects";
                        }
                    }catch(Exception e)
                    {

                    }
                    return search_result;
		} catch (Exception e) {
			return search_result;
		}
	}
        
        public JSONObject getSearchStatement(String searchText,JSONObject statement) throws JSONException
        {
            SearchUtilities searchUtilities=new SearchUtilities();
            JSONObject searchObj=new JSONObject();
            searchObj.put("$regex",searchUtilities.get_and_regex_saerch_staring(searchText));
            searchObj.put("$options","is");
            statement.put("primary_tag",searchObj);
            return statement;
        }
        public JSONObject getRelatedSearchStatement(String searchText,JSONObject excludeObj,JSONObject statement) throws JSONException
        {
            SearchUtilities searchUtilities=new SearchUtilities();
            
            JSONObject excludeTemp=new JSONObject();
            excludeTemp=excludeTemp.put("$not", excludeObj.getJSONObject("primary_tag"));//placing not operator to exclude this ads..
            excludeObj.put("primary_tag", excludeTemp);
            ShowErrorLogOfPostResuest.traceMessaage+="\nstatement:"+statement.toString();
            
            JSONObject searchObj=new JSONObject();
            ShowErrorLogOfPostResuest.traceMessaage+="excludeobj:"+excludeObj.toString()+"\nSearch Text:"+searchText;
            searchText=searchText.replaceAll("\\.*[^a-zA-Z0-9]"," ");
            String []tags=searchText.split("\\s++");
            for(int i=0;i<tags.length;i++) searchText+=" "+searchUtilities.get_formated_string(tags[i]);
            ShowErrorLogOfPostResuest.traceMessaage+="\nFormatted Search Text:"+searchText;
            searchObj.put("$regex",searchUtilities.get_or_regex_saerch_staring(searchText));
            searchObj.put("$options","is");
            ShowErrorLogOfPostResuest.traceMessaage+="\nsearchObjString:"+searchObj.toString();
            
            JSONObject searchObjPrimary=new JSONObject();
            searchObjPrimary.put("primary_tag", searchObj);
            ShowErrorLogOfPostResuest.traceMessaage+="\nPrimaryObject:"+searchObjPrimary.toString();
            
            JSONArray relatedSearchArray=new JSONArray();
            relatedSearchArray.put(excludeObj);
            relatedSearchArray.put(searchObjPrimary);
            ShowErrorLogOfPostResuest.traceMessaage+="\nstatement:"+statement.toString()+"relatedSearchArray:"+relatedSearchArray;
            statement=new JSONObject();
            statement.put("$and",relatedSearchArray); //exclude the previous result and include the new results...
            ShowErrorLogOfPostResuest.traceMessaage+=" statement:"+statement.toString();
            return statement;
        }
	public void updatefields(DB db, JSONObject jo1, JSONObject jo2)
			throws JSONException {
		DBObject statement = (DBObject) JSON.parse(jo1.toString());
		JSONObject jobj = new JSONObject();
		jobj.put("$set", jo2);
		DBObject updatefield = (DBObject) JSON.parse(jobj.toString());
		int len = catagory.length;
		for (int i = 0; i < len; i++) {
			// get a single collection
			DBCollection collection = db.getCollection("searchdet"
					+ catagory[i]);
			collection.update(statement, updatefield, false, true);
		}
		for (int i = 0; i < len; i++) {
			// get a single collection
			DBCollection collection = db
					.getCollection("otherdet" + catagory[i]);
			collection.update(statement, updatefield, false, true);
		}
		DBCollection collection = db.getCollection("registration");
		collection.update(statement, updatefield, false, true);
	}

	public void unregister(DB db, JSONObject jsonobj) throws JSONException {
		if (removedevice(db, jsonobj))
			removedocuments(db, jsonobj.toString());
	}

	public boolean removedevice(DB db, JSONObject jsonobj) {
		// using userid to find & remove....
		try {
			DBCollection collection = db.getCollection("registration");
			DBObject dbobj = (DBObject) JSON.parse(jsonobj.toString());
			collection.remove(dbobj);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void removedocuments(DB db, String statement) {
		// using userid to find & remove....
		int len = catagory.length;
		DBObject dbobj = (DBObject) JSON.parse(statement);
		for (int i = 0; i < len; i++) {
			DBCollection collection = db.getCollection("searchdet"
					+ catagory[i]);
			collection.remove(dbobj);
		}
		for (int i = 0; i < len; i++) {
			DBCollection collection = db
					.getCollection("otherdet" + catagory[i]);
			collection.remove(dbobj);
		}
		DBCollection collection = db.getCollection("notification_collection");
		collection.remove(dbobj);
	}

	public void removedocuments(DB db, String statement, String givencat) {
		// using productid to find & remove....
		DBObject dbobj = (DBObject) JSON.parse(statement);

		DBCollection collection = db.getCollection("searchdet" + givencat);
		collection.remove(dbobj);
		DBCollection collection1 = db.getCollection("otherdet" + givencat);
		collection1.remove(dbobj);
		DBCollection collection2 = db.getCollection("notification_collection");
		collection2.remove(dbobj);
	}

	// send messages to one devices ........
	public boolean sendmessage(ServletConfig config, String registrationId) {
		final Sender sender = newSender(config);
		Message message = BUILDED_MESSAGE;
		try {
			Result result = sender.send(message, registrationId, 5);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// out.print("Single Message sending failed");
			return false;
		}
	}

	// send message to all the registered devices..........
	public boolean sendmessage(DB db, PrintWriter out, ServletConfig config) {
		final Sender sender = newSender(config);
		List<String> devices = getdevices(db);
		if (devices.isEmpty()) {
			out.println("\n--NO DEVICE--\nMessage ignored as there is no device registered!\n");
		} else {
			out.println("\n--DEVICES-- " + devices.size() + "--\n");
			int total = devices.size();
			List<String> partialDevices = new ArrayList<String>(total);
			int counter = 0;
			// int tasks = 0;
			for (String device : devices) {
				counter++;
				out.println(device + "\n");
				partialDevices.add(device);
				int partialSize = partialDevices.size();
				if (partialSize == MULTICAST_SIZE || counter == total) {
					asyncSend(db, partialDevices, out, sender);
					partialDevices.clear();
					// tasks++;
				}
			}
		}
		return true;
	}

	private void asyncSend(final DB db, List<String> partialDevices,
			final PrintWriter out, final Sender sender) {
		// make a copy

		final Executor threadPool = Executors.newFixedThreadPool(5);
		final List<String> devices = new ArrayList<String>(partialDevices);
		threadPool.execute(new Runnable() {

			public void run() {
				Message message = BUILDED_MESSAGE;
				MulticastResult multicastResult;
				try {
					multicastResult = sender.send(message, devices, 5);
					out.println("\nMULTICAST:\n" + multicastResult.toString()
							+ "\n---MULTIFINISHED---\n");
					// out.println("MULTICAST:" + multicastResult.toString());
				} catch (IOException e) {
					// logger.log(Level.SEVERE, "Error posting messages", e);
					return;
				}
				List<Result> results = multicastResult.getResults();
				// analyze the results
				for (int i = 0; i < devices.size(); i++) {
					String regId = devices.get(i);
					Result result = results.get(i);
					String messageId = result.getMessageId();
					if (messageId != null) {
						// out.println("MESSAGE ID:"+messageId);
						out.println("\nUNIQ MESSAGE:\nRGID:" + regId
								+ "\nMESSID:" + messageId + "\n");
						String canonicalRegId = result
								.getCanonicalRegistrationId();
						if (canonicalRegId != null) {
							// same device has more than on registration id:
							// update it
							out.println("\nUNIQ CANONICAL:\n" + canonicalRegId
									+ "\n");
							try {

								// out.println("CANONICAL ID:"+canonicalRegId);
								JSONObject jo1 = new JSONObject();
								jo1.put("user_id", regId);
								JSONObject jo2 = new JSONObject();
								jo2.put("user_id", canonicalRegId);
								updatefields(db, jo1, jo2);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						String error = result.getErrorCodeName();

						out.println("\nUNIQ ERROR:\n" + error + "\n");
						if (error.equals(Constants.ERROR_NOT_REGISTERED)
								|| error.equals(Constants.ERROR_INVALID_REGISTRATION)) {
							try {
								unregister(db,
										new JSONObject().put("user_id", regId));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							// logger.severe("Error sending message to " + regId
							// + ": " + error);
						}
					}
				}
				out.println("\n---MESSAGE RESULT FINISHED---\n");
			}
		});
	}

	public String getsearchtag(String tagname) {
		// TODO Auto-generated method stub

		return null;
	}

	public JSONArray optimize_json_array(JSONArray search_result)
			throws JSONException {
		// TODO Auto-generated method stub
		JSONArray joptimize = new JSONArray();
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		long len = search_result.length();
		int status = 0;
		for (int i = 0; i < len; i++) {
			try {
				String _id = search_result.getJSONObject(i).getString("_id");

				try {
					status = hm.get(_id);
				} catch (Exception e) {
					status = 0;
				}
				if (status == 0) {
					hm.put(_id, 1);
					joptimize.put(search_result.getJSONObject(i));
				}
			} catch (Exception e) {
				joptimize.put(e.toString());
			}
		}
		return joptimize;
	}
}
