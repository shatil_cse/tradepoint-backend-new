
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;

@SuppressWarnings("serial")
public class Getalldata extends HttpServlet {
	String title="";	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain; charset=utf8");
		PrintWriter out = resp.getWriter();
		out.println("Success\n");
		String catagory[]=new String[]{"Furnitures","Sports","Electronics","Mobile Phones","Others"};
		JSONObject jsonobj=new JSONObject();
		try {
			jsonobj.put("statement",new JSONObject());
			JSONArray jarr=new JSONArray(catagory);
			jsonobj.put("catagory", jarr);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(title);
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		out.println("AUTH:"+db.isAuthenticated());
		comutil.brawsingdatabase(db,out,jsonobj);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		//doGet(req, resp);
                try{
                    boolean text_search=false;
                    String collname="browse_catagories";
                    String index_name="tag_name";
                    resp.setContentType("application/json; charset=utf8");
                    PrintWriter out = resp.getWriter();
                    CommonUtilities comutil=new CommonUtilities();
                    JSONObject jsonobj=new JSONObject();
                    jsonobj=comutil.getJSON(req,out);
                    //System.out.println(title);
                    ShowErrorLogOfPostResuest.lastRequest=jsonobj.toString();
                    try {
                            collname=jsonobj.getString("collection_name");
                            text_search=jsonobj.getBoolean("text_search");
                            index_name=jsonobj.getString("index_name");
                    } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            ShowErrorLogOfPostResuest.errorMessages+="\nJSONEXCEPTION Error on getalldata: "+e.toString();
                            
                    }
                    if(collname.startsWith("browse_catagories")) comutil.brawsingdatabase(comutil.getdb(out,getServletContext()),out,jsonobj);
                    else 
                    {
                            if(text_search==false) comutil.brawsingdatabase(comutil.getdb(out,getServletContext()),collname,out,jsonobj);
                            else comutil.searching_text(comutil.getdb(out,getServletContext()),collname,out,jsonobj,index_name);
                    }
                }catch(Exception e)
                {
                    ShowErrorLogOfPostResuest.errorMessages+="\nError on getalldata: "+e.toString();
                }
                ShowErrorLogOfPostResuest.lastPage="getalldata";
                
	}
}