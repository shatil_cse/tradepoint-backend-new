import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.mongodb.DB;

@SuppressWarnings("serial")
public class SendAllMessagesServlet extends HttpServlet {
  String status="This is status...";
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws IOException, ServletException {
	  PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		try {
			comutil.inserttodatabase(db,out, comutil.getJSON(req,out));
			JSONObject jobj=comutil.messagestatement(true, false, false, false,false);
			jobj.put("settings",comutil.getsettingobject());
			comutil.build_message(jobj.toString());
			comutil.sendmessage(db, out,getServletConfig());
		} catch (Exception e) {
			out.println("Error In Posting your Request:\n"+e.toString());
		}
		req.setAttribute(HomeServlet.ATTRIBUTE_STATUS, status.toString());
		getServletContext().getRequestDispatcher("/home").forward(req, resp);
  }
}
