import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

/**
 * Servlet that adds display number of devices and button to send a message.
 * <p>
 * This servlet is used just by the browser (i.e., not device) and contains the
 * main page of the demo app.
 */
@SuppressWarnings("serial")
public class HomeServlet extends HttpServlet {
	String title;
	MongoClient mongoClient;
	DB db;
	JSONObject jsonobj;
	String connectString = "false";
	static final String ATTRIBUTE_STATUS = "status";

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		connectString = connecttodatabase(null);
	}

	/**
	 * Displays the existing messages and offer the option to send a new one.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		connectString=connecttodatabase(out);
		out.print(connectString);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet(req, resp);
	}

	private List<String> getdevices(PrintWriter out) {
		// TODO Auto-generated method stub
		List<String> devices = new ArrayList<String>();
		DBCollection coll = db.getCollection("registration");
		DBObject statement = (DBObject) JSON.parse("{}");
		DBObject fields = (DBObject) JSON.parse("{\"date\":0,\"time\":0}");
		DBCursor dbcur = coll.find(statement, fields);
		while (dbcur.hasNext()) {
			String st = dbcur.next().toString();
			try {
				st = new JSONObject(st).getString("_id");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.println(st);
			devices.add(st);
		}
		return devices;
	}

	public String connecttodatabase(PrintWriter out) {
		try {
			CommonUtilities comutil = new CommonUtilities();
			db = comutil.getdb(out,getServletContext());
			if (db.isAuthenticated())
				return "Connected to data base";
			else
				return "Failed to connect";
		} catch (Exception e) {
			return "Failed to connect";
		}
	}
}