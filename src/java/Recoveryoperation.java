
import java.io.PrintWriter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class Recoveryoperation {
	String catagory[]=new String[]{"Furnitures","Sports","Electronics","Mobile Phones","Others"};
	HttpServletResponse response;
	public Recoveryoperation(HttpServletResponse response) {
		// TODO Auto-generated constructor stub
		this.response=response;
	}
	public JSONObject get_error_object(){
		JSONObject rset=new JSONObject();
		try {
			rset.put("notification_time", System.currentTimeMillis());
			rset.put("status_int", 500);
			return rset;
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			return new JSONObject();
		}
	}
	public boolean check_reg_id(String reg_id, String _id,DB db){
		// TODO Auto-generated method stub
		try{
			JSONObject jobj=new JSONObject();
			jobj.put("reg_id", reg_id);
			jobj.put("_id", _id);
			DBObject statement=(DBObject)JSON.parse(jobj.toString());
			JSONObject json=new JSONObject(db.getCollection("registration").findOne(statement).toString());
			if(json.getString("reg_id").equals(reg_id))return true;
			else return false;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	public String save_reg(DB db,JSONObject jobj,PrintWriter out)
	{
		//Set initial pin & mail if provided
		try{
			String coll_name="registration";
			// get a single collection
			DBCollection collection = db.getCollection(coll_name);
			DBObject statement=(DBObject) JSON.parse(jobj.getJSONObject("statement").toString());
			//creates index ....
			JSONObject rset=new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			DBCursor cursorDoc=collection.find(statement);
			if(cursorDoc.hasNext())
			{
				JSONObject fres=new JSONObject(cursorDoc.next().toString());
				boolean has_pin=fres.has("pin_configured");
				if(!has_pin)
				{
					JSONObject gattach=jobj.getJSONObject("attachment");
					fres.put("attachment", gattach);
					fres.put("pin_configured", true);
					DBObject dbres=(DBObject) JSON.parse(fres.toString());
					collection.save(dbres);
					rset.put("status_int", 250);
				}
				else if(!fres.getBoolean("pin_configured"))
				{
					JSONObject gattach=jobj.getJSONObject("attachment");
					fres.put("attachment", gattach);
					fres.put("pin_configured", true);
					DBObject dbres=(DBObject) JSON.parse(fres.toString());
					collection.save(dbres);
					rset.put("status_int", 250);
				}
				else rset.put("status_int", 275);
			}
			else rset.put("status_int", 99);
			out.write(rset.toString());
			return rset.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			out.write(get_error_object().toString());
			return e.toString();
		}
	}
	public boolean pin_check(JSONObject fatt,JSONObject gatt) throws JSONException
	{
		try{
			if(gatt.getLong("saved_pin")==fatt.getLong("saved_pin")) return true;
			else return false;
		}catch(Exception e)
		{
			return false;
		}
	}
	public boolean mail_check(JSONObject fatt,JSONObject gatt) throws JSONException
	{
		try{
			if(gatt.getString("saved_mail").equals(fatt.getString("saved_mail"))) return true;
			else return false;
		}catch(Exception e)
		{
			return false;
		}
	}
	public boolean key_check(JSONObject fatt,JSONObject gatt) throws JSONException
	{
		try{
		if(gatt.getLong("saved_server_key")==fatt.getLong("saved_server_key")) return true;
		else return false;
		}catch(Exception e)
		{
			return false;
		}
	}
	public String change_data(DB db, JSONObject jobj, PrintWriter out) {
		// TODO Auto-generated method stub
		
		try{
			JSONObject rset=new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			
			String coll_name="registration";
			DBCollection collection = db.getCollection(coll_name);
			DBObject statement=(DBObject) JSON.parse(jobj.getJSONObject("statement").toString());
			DBCursor cursorDoc=collection.find(statement);
			if(cursorDoc.hasNext())
			{
				JSONObject fres=new JSONObject(cursorDoc.next().toString());
				JSONObject gattach=jobj.getJSONObject("attachment");
				JSONObject fattach=fres.getJSONObject("attachment");
				int data_change_st=jobj.getInt("data_change_st");
				if(data_change_st==1)
				{
					//set new pin using previous pin
					if(pin_check(fattach, gattach))
					{
						fattach.put("saved_pin", gattach.getLong("new_pin"));
						fres.put("attachment", fattach);
						DBObject dbres=(DBObject) JSON.parse(fres.toString());
						collection.save(dbres);
						rset.put("status_int", 250);
					}
					else rset.put("status_int", 276);
				}
				else if(data_change_st==2)
				{
					// set new mail by providing previous pin
					if(pin_check(fattach, gattach))
					{
						fattach.put("saved_mail", gattach.getString("new_mail"));
						fres.put("attachment", fattach);
						DBObject dbres=(DBObject) JSON.parse(fres.toString());
						collection.save(dbres);
						rset.put("status_int", 250);
					}
					else rset.put("status_int", 276);
				}
				else if(data_change_st==3)
				{
					//send pin to mail address if forget pin
					if(fattach.has("saved_mail"))
					{
						String emailadd=fattach.getString("saved_mail");
						long pin=fattach.getLong("saved_pin");
						send_email(emailadd,pin,true);
						rset.put("status_int", 250);
					}
					else rset.put("status_int", 277);
				}
				else if(data_change_st==4)
				{
					//get serverkey on device.............. if do not have mail and need to change device
					if(pin_check(fattach, gattach))
					{
						long key=get_serveer_key();
						rset.put("saved_server_key", key);
						fattach.put("saved_server_key", key);
						fres.put("attachment", fattach);
						DBObject dbres=(DBObject) JSON.parse(fres.toString());
						collection.save(dbres);
						rset.put("status_int", 250);
					}
					else rset.put("status_int", 277);
				}
				else if(data_change_st==5)
				{
					//get serverkey on mail..............if have mail and need to change device
					if(fattach.has("saved_mail"))
					{
						String emailadd=fattach.getString("saved_mail");
						long key=get_serveer_key();
						send_email(emailadd,key,false);
						fattach.put("saved_server_key", key);
						fres.put("attachment", fattach);
						DBObject dbres=(DBObject) JSON.parse(fres.toString());
						collection.save(dbres);
						rset.put("status_int", 250);
					}
					else rset.put("status_int", 277);
				}
			}
			else rset.put("status_int", 99);
			out.write(rset.toString());
			return rset.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			out.write(get_error_object().toString());
			return e.toString();
		}
	}
	
	private long get_serveer_key() {
		// TODO Auto-generated method stub
		return 1000;
	}

	public String lost_device_recovery(DB db, JSONObject gres, PrintWriter out) {
		// TODO Auto-generated method stub
		try{
			JSONObject rset=new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			
			String coll_name="registration";
			DBCollection collection = db.getCollection(coll_name);
			//find this device object..
			JSONObject gstatement=gres.getJSONObject("statement");
			DBObject statement=(DBObject) JSON.parse(gstatement.toString());
			DBCursor cursorDoc=collection.find(statement);
			if(cursorDoc.hasNext())
			{
				JSONObject fres=new JSONObject(cursorDoc.next().toString());
				JSONObject gattach=gres.getJSONObject("attachment");
				JSONObject fattach=fres.getJSONObject("attachment");
				//check previous deice pin mail id server key
				//incomplete work here check for this device pin..........
				if(pin_check(fattach, gattach))
				{
					//find lost device object............
					JSONObject lost_statement=new JSONObject();
					String emailadd=gattach.getString("lost_mail");
					lost_statement.put("attachment.saved_mail",emailadd);
					lost_statement.put("attachment.saved_pin", gattach.getLong("lost_pin"));
					DBObject statement2=(DBObject) JSON.parse(lost_statement.toString());
					DBCursor dbc=collection.find(statement2);
					
					if(dbc.hasNext())
					{
						long key=0;
						key=get_serveer_key();
						send_email(emailadd,key, false);
						fres.put("saved_server_key",key);
						DBObject dbres2=(DBObject) JSON.parse(fres.toString());
						collection.save(dbres2);
						rset.put("status_int", 250);
					}
					else rset.put("status_int", 280);
				}
				else rset.put("status_int", 280);
			}
			else rset.put("status_int", 99);
			out.write(rset.toString());
			return rset.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			out.write(get_error_object().toString());
			return e.toString();
		}
	}
	
	public String recover_device(DB db, JSONObject gres, PrintWriter out) {
		// TODO Auto-generated method stub
		try{
			JSONObject rset=new JSONObject();
			rset.put("notification_time", System.currentTimeMillis());
			String coll_name="registration";
			DBCollection collection = db.getCollection(coll_name);
			//find this device object..
			JSONObject gstatement=gres.getJSONObject("statement");
			DBObject statement=(DBObject) JSON.parse(gstatement.toString());
			DBCursor cursorDoc=collection.find(statement);
			if(cursorDoc.hasNext())
			{
				JSONObject fres=new JSONObject(cursorDoc.next().toString());
				JSONObject gattach=gres.getJSONObject("attachment");
				JSONObject fattach=fres.getJSONObject("attachment");
				//check previous deice pin mail id server key
				//incomplete work here check for this device pin..........
				if(pin_check(fattach, gattach))
				{
					//find lost device object............
					JSONObject lost_statement=new JSONObject();
					lost_statement.put("attachment.saved_server_key",gattach.getLong("saved_server_key"));
					lost_statement.put("attachment.saved_pin", gattach.getLong("pre_pin"));
					DBObject statement2=(DBObject) JSON.parse(lost_statement.toString());
					DBCursor dbc=collection.find(statement2);
					if(dbc.hasNext())
					{
						JSONObject nres=new JSONObject(dbc.next().toString());
						//add old device setting with this registration id.....
						nres.put("attachment_old", nres.getJSONObject("attachment"));
						fres.put("attachment_old", fres.getJSONObject("attachment"));
						//leave note on devices...........
						nres.put("device_changed_from",fres.getString("_id"));
						fres.put("device_changed_to",nres.getString("_id"));
						//update the fields user_id................
						String reg_id=nres.getString("reg_id");
						String _id=nres.getString("_id");
						nres.put("reg_id", fres.getString("reg_id"));
						fres.put("reg_id", reg_id);
						
						//remove saved_server_key from nres...
						JSONObject atta=nres.getJSONObject("attachment");
						atta.remove("saved_server_key");
						nres.put("attachment", atta);
						
						//save registration information....
						DBObject dbres=(DBObject) JSON.parse(nres.toString());
						collection.save(dbres);
						DBObject dbres2=(DBObject) JSON.parse(fres.toString());
						collection.save(dbres2);
						rset.put("_id", _id);
						rset.put("user_name", nres.getString("user_name"));
						rset.put("status_int", 250);
						//send a message to fres reg_id that his _id has been changed...........
					}
					else rset.put("status_int", 280);
				}
				else rset.put("status_int", 280);
			}
			else rset.put("status_int", 99);
			out.write(rset.toString());
			return rset.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			out.write(get_error_object().toString());
			return e.toString();
		}
	}
	public boolean updatefields(DB db,JSONObject select,JSONObject update) throws JSONException
	{
    	DBObject statement=(DBObject) JSON.parse(select.toString());
		JSONObject jobj=new JSONObject();
		jobj.put("$set", update);
		DBObject updatefield=(DBObject) JSON.parse(jobj.toString());
		int len=catagory.length;
		for(int i=0;i<len;i++)
		{
			  DBCollection collection = db.getCollection("searchdet"+catagory[i]);
			  collection.update(statement, updatefield,false,true);
		}
		for(int i=0;i<len;i++)
		{
			  DBCollection collection = db.getCollection("otherdet"+catagory[i]);
			  collection.update(statement, updatefield,false,true);
		}
		DBCollection collection = db.getCollection("notification_collection");
		collection.update(statement, updatefield,false,true);
		return true;
	}
	public boolean send_email(String emailadd,long key,boolean pin) {
		// TODO Auto-generated method stub
		// Recipient's email ID needs to be mentioned.
		String to = emailadd;
		// Sender's email ID needs to be mentioned
		String from = "shatil200@gmail.com";

		// Assuming you are sending email from localhost
		String host ="localhost";

		// Get system properties
		Properties properties = System.getProperties();
		properties.setProperty("mail.user", "shatil200@gmail.com");
		properties.setProperty("mail.password", "shatil111991");
		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);
		// Set response content type
		response.setContentType("text/html");
		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);
			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));
			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			if(pin)
			{
				// Set Subject: header field
				message.setSubject("PIN received...");
				// Now set the actual message
				// Send the actual HTML message, as big as you like
		         message.setContent("<h3>Your PIN is :</h3>"+key,
		                            "text/html" );	
			}
			else 
			{
				// Set Subject: header field
				message.setSubject("Server key received...");
				// Now set the actual message
				// Send the actual HTML message, as big as you like
		         message.setContent("<h3>Your Server key is :</h3>"+key,
		                            "text/html" );
				
			}
	         // Send message
			Transport.send(message);

		} catch (MessagingException mex) {
			mex.printStackTrace();
			return false;
		}
		return true;
	}
}
