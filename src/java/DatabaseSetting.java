import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;

@SuppressWarnings("serial")
public class DatabaseSetting extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// Set response content type
		resp.setContentType("text/plain; charset=utf8");
		PrintWriter out = resp.getWriter();
		out.println("Success\n");
		String catagory[]=new String[]{"Sports","Electronics"};
		JSONObject jsonobj=new JSONObject();
		try {
			jsonobj.put("statement",new JSONObject());
			JSONArray jarr=new JSONArray(catagory);
			jsonobj.put("catagory", jarr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		out.println("AUTH:"+db.isAuthenticated());
		comutil.brawsingdatabase(db,out,jsonobj);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		DB db=comutil.getdb(out,getServletContext());
		try {
			JSONObject jobj=comutil.getJSON(req,out);
			comutil.inserttodatabase(db,out,jobj);
		} catch (Exception e) {
			out.println("Error In Posting your Request:\n"+e.toString());
		}
	}	
}
