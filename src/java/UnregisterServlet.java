
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
@SuppressWarnings("serial")
public class UnregisterServlet extends HttpServlet {
@Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	  resp.setContentType("application/json; charset=utf8");
		PrintWriter out = resp.getWriter();
		CommonUtilities comutil=new CommonUtilities();
		try {
			JSONObject jsonobj;
			jsonobj=comutil.getJSON(req, out);
			comutil.unregister(comutil.getdb(out,getServletContext()),jsonobj);
			} catch (Exception e) {
			out.println("Error In Posting your Request:\n"+e.toString());
		}
	}
}
